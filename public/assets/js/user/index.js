$(function () {


    fetchToken()
    setupUIListeners()

    let data = {
        "age": 70,
        "Height": 150,
        "Weight": 70,
        "BMI": 0,
        "sex": "Male",
        "ScreenTimeHours": "Two_Three",
        "SystolicBP": "Low",
        "CoffeeConsumption": "TwoThree",
        "Education": "Intermediate",
        "Smoking": "Never",
        "AlcoholConsumption": "None",
        "SleepDuration": "Long",
        "JobStrain": "No",
        "FamilyHistoryCHD": "No",
        "FamilyHistoryT2DM": "No",
        "MediterraneanDietAdherence": "High",
        "Obesity": "Overweight",
        "HDLCholesterol": "Low",
        "PhysicalActivity": "Active",
    }


    function calcBMI(height, weight) {
        console.log(height, weight)
        height = height / 100
        return weight / (height * height)
    }

    function setupUIListeners() {


        // AGE
        $('#Age').slider();
        $("#Age").on("slide", function (slideEvt) {
            $("#AgeSliderVal").text(slideEvt.value);
            data.age = slideEvt.value
            console.log(data)

        });

        // GENDER

        $('#Male').click(function () {
            $('#Male').addClass('active')
            $('#Female').removeClass('active')
            data.sex = "Male"

        })

        $('#Female').click(function () {
            $('#Female').addClass('active')
            $('#Male').removeClass('active')
            data.sex = "Female"

        })

        //HEIGHT
        $('#Height').slider();
        $("#Height").on("slide", function (slideEvt) {
            $("#HeightSliderVal").text(slideEvt.value);
            data.Height = slideEvt.value
            console.log(data)

            data.BMI = calcBMI(data.Height, data.Weight)

        });


        // EDUCATION
        $('#Low').click(function () {
            $('#Low').addClass('active')
            $('#Intermediate').removeClass('active')
            $('#High').removeClass('active')
            data.Education = "Low"

        })

        $('#Intermediate').click(function () {
            $('#Low').removeClass('active')
            $('#Intermediate').addClass('active')
            $('#High').removeClass('active')
            data.Education = "Intermediate"

        })
        $('#High').click(function () {
            $('#Low').removeClass('active')
            $('#Intermediate').removeClass('active')
            $('#High').addClass('active')
            data.Education = "High"

        })

        $('#FamilyHistoryCHD').click(function(){
            if($(this).prop("checked") == true){
               data.FamilyHistoryCHD = "Yes"
            }
            else if($(this).prop("checked") == false){
                data.FamilyHistoryCHD = "No"

            }
        });

        $('#FamilyHistoryT2DM').click(function(){
            if($(this).prop("checked") == true){
               data.FamilyHistoryT2DM = "Yes"
            }
            else if($(this).prop("checked") == false){
                data.FamilyHistoryT2DM = "No"

            }
        });


        // Verify Form
        $('#enter-playground').click(function () {

            let stringData = JSON.stringify(data)
            sessionStorage.setItem('userData', stringData)
            window.location.href = window.location + "playground.html"; // Put url to redirect

        })

    }



})