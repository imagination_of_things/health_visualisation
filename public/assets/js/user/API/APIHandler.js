// First get access token

//Then can make requests for services

function returnDemoData() {
  let random = Math.random()
  if (random < 0.5) {
    return {
      "CurrentHealthScore": 78.21132,
      "AllCases": [{
          "Variable": "ScreenTimeHours",
          "Value": "LT_two",
          "CurrentHabit": "No",
          "HealthScore": 92.1908,
          "HealthGain": 35.8407
        },
        {
          "Variable": "SystolicBP",
          "Value": "Low",
          "CurrentHabit": "No",
          "HealthScore": 93.3749,
          "HealthGain": 30.4063
        },
        {
          "Variable": "Smoking",
          "Value": "Never",
          "CurrentHabit": "No",
          "HealthScore": 94.7832,
          "HealthGain": 23.9426
        },
        {
          "Variable": "SystolicBP",
          "Value": "Mid_Low",
          "CurrentHabit": "No",
          "HealthScore": 94.7978,
          "HealthGain": 23.8757
        },
        {
          "Variable": "CoffeeConsumption",
          "Value": "ThreeFour",
          "CurrentHabit": "No",
          "HealthScore": 95.712,
          "HealthGain": 19.68
        },
        {
          "Variable": "CoffeeConsumption",
          "Value": "FourMore",
          "CurrentHabit": "No",
          "HealthScore": 96.9456,
          "HealthGain": 14.0185
        },
        {
          "Variable": "SystolicBP",
          "Value": "Mid_High",
          "CurrentHabit": "No",
          "HealthScore": 97.3756,
          "HealthGain": 12.0449
        },
        {
          "Variable": "SleepDuration",
          "Value": "Normal",
          "CurrentHabit": "No",
          "HealthScore": 97.5355,
          "HealthGain": 11.3107
        },
        {
          "Variable": "HDLCholesterol",
          "Value": "High",
          "CurrentHabit": "No",
          "HealthScore": 97.5799,
          "HealthGain": 11.1072
        },
        {
          "Variable": "PhysicalActivity",
          "Value": "Active",
          "CurrentHabit": "Yes",
          "HealthScore": 98.6658,
          "HealthGain": 6.1233
        },
        {
          "Variable": "ScreenTime",
          "Value": "Less_one",
          "CurrentHabit": "Yes",
          "HealthScore": 99.2751,
          "HealthGain": 3.3269
        },
        {
          "Variable": "CoffeeConsumption",
          "Value": "TwoThree",
          "CurrentHabit": "Yes",
          "HealthScore": 99.7546,
          "HealthGain": 1.1265
        },
        {
          "Variable": "SleepDuration",
          "Value": "Long",
          "CurrentHabit": "Yes",
          "HealthScore": 99.7955,
          "HealthGain": 0.9387
        },
        {
          "Variable": "ScreenTimeHours",
          "Value": "Two_Four",
          "CurrentHabit": "Yes",
          "HealthScore": 99.8698,
          "HealthGain": 0.5976
        },
        {
          "Variable": "ScreenTimeHours",
          "Value": "GT_Four",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "SystolicBP",
          "Value": "High",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "CoffeeConsumption",
          "Value": "LessOne",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "Smoking",
          "Value": "Former",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "AlcoholConsumption",
          "Value": "Normal",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "SleepDuration",
          "Value": "Short",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "JobStrain",
          "Value": "No",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "MediterraneanDietAdherence",
          "Value": "High",
          "CurrentHabit": "No",
          "HealthScore": 56.4226,
          "HealthGain": 0
        },
        {
          "Variable": "Obesity",
          "Value": "None",
          "CurrentHabit": "No",
          "HealthScore": 56.4226,
          "HealthGain": 0
        },
        {
          "Variable": "HDLCholesterol",
          "Value": "Medium",
          "CurrentHabit": "No",
          "HealthScore": 56.4226,
          "HealthGain": 0
        },
        {
          "Variable": "PhysicalActivity",
          "Value": "Moderately_Active",
          "CurrentHabit": "No",
          "HealthScore": 56.4226,
          "HealthGain": 0
        },
        {
          "Variable": "ScreenTime",
          "Value": "One_Two",
          "CurrentHabit": "No",
          "HealthScore": 56.4226,
          "HealthGain": 0
        },
        {
          "Variable": "MediterraneanDietAdherence",
          "Value": "Medium",
          "CurrentHabit": "No",
          "HealthScore": 56.1129,
          "HealthGain": -1.4217
        },
        {
          "Variable": "ScreenTime",
          "Value": "Two_Three",
          "CurrentHabit": "No",
          "HealthScore": 55.8648,
          "HealthGain": -2.5604
        },
        {
          "Variable": "CoffeeConsumption",
          "Value": "OneTwo",
          "CurrentHabit": "No",
          "HealthScore": 55.8522,
          "HealthGain": -2.618
        },
        {
          "Variable": "MediterraneanDietAdherence",
          "Value": "Low",
          "CurrentHabit": "No",
          "HealthScore": 55.2133,
          "HealthGain": -5.5504
        },
        {
          "Variable": "HDLCholesterol",
          "Value": "Low",
          "CurrentHabit": "No",
          "HealthScore": 54.8087,
          "HealthGain": -7.4071
        },
        {
          "Variable": "JobStrain",
          "Value": "Yes",
          "CurrentHabit": "No",
          "HealthScore": 54.5974,
          "HealthGain": -8.377
        },
        {
          "Variable": "PhysicalActivity",
          "Value": "Inactive",
          "CurrentHabit": "No",
          "HealthScore": 53.7775,
          "HealthGain": -12.1402
        },
        {
          "Variable": "ScreenTime",
          "Value": "Gt_three",
          "CurrentHabit": "No",
          "HealthScore": 53.519,
          "HealthGain": -13.3263
        },
        {
          "Variable": "Obesity",
          "Value": "Overweight",
          "CurrentHabit": "No",
          "HealthScore": 53.253,
          "HealthGain": -14.5473
        },
        {
          "Variable": "AlcoholConsumption",
          "Value": "None",
          "CurrentHabit": "No",
          "HealthScore": 52.721,
          "HealthGain": -16.9888
        },
        {
          "Variable": "AlcoholConsumption",
          "Value": "Heavy",
          "CurrentHabit": "No",
          "HealthScore": 52.2578,
          "HealthGain": -19.1148
        },
        {
          "Variable": "Obesity",
          "Value": "Obese",
          "CurrentHabit": "No",
          "HealthScore": 46.8374,
          "HealthGain": -43.9921
        },
        {
          "Variable": "Smoking",
          "Value": "Current",
          "CurrentHabit": "No",
          "HealthScore": 40.4004,
          "HealthGain": -73.5348
        }
      ]
    }
  } else {


    return {
      "CurrentHealthScore": 85.31923,
      "AllCases": [{
          "Variable": "ScreenTimeHours",
          "Value": "LT_two",
          "CurrentHabit": "No",
          "HealthScore": 94.1551,
          "HealthGain": 39.8136
        },
        {
          "Variable": "CoffeeConsumption",
          "Value": "ThreeFour",
          "CurrentHabit": "No",
          "HealthScore": 95.8743,
          "HealthGain": 28.103
        },
        {
          "Variable": "CoffeeConsumption",
          "Value": "FourMore",
          "CurrentHabit": "No",
          "HealthScore": 97.368,
          "HealthGain": 17.9282
        },
        {
          "Variable": "SleepDuration",
          "Value": "Normal",
          "CurrentHabit": "No",
          "HealthScore": 98.0035,
          "HealthGain": 13.5997
        },
        {
          "Variable": "AlcoholConsumption",
          "Value": "Normal",
          "CurrentHabit": "No",
          "HealthScore": 98.016,
          "HealthGain": 13.5142
        },
        {
          "Variable": "HDLCholesterol",
          "Value": "High",
          "CurrentHabit": "No",
          "HealthScore": 98.0776,
          "HealthGain": 13.095
        },
        {
          "Variable": "Obesity",
          "Value": "None",
          "CurrentHabit": "No",
          "HealthScore": 98.1332,
          "HealthGain": 12.7159
        },
        {
          "Variable": "HDLCholesterol",
          "Value": "Medium",
          "CurrentHabit": "No",
          "HealthScore": 99.218,
          "HealthGain": 5.3266
        },
        {
          "Variable": "SleepDuration",
          "Value": "Short",
          "CurrentHabit": "No",
          "HealthScore": 99.8462,
          "HealthGain": 1.0479
        },
        {
          "Variable": "ScreenTimeHours",
          "Value": "Two_Four",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "SystolicBP",
          "Value": "Low",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "CoffeeConsumption",
          "Value": "TwoThree",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "Smoking",
          "Value": "Never",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "AlcoholConsumption",
          "Value": "None",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "SleepDuration",
          "Value": "Long",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "JobStrain",
          "Value": "No",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "MediterraneanDietAdherence",
          "Value": "High",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "Obesity",
          "Value": "Overweight",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "HDLCholesterol",
          "Value": "Low",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "PhysicalActivity",
          "Value": "Active",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "ScreenTime",
          "Value": "Less_one",
          "CurrentHabit": "Yes",
          "HealthScore": 100,
          "HealthGain": 0
        },
        {
          "Variable": "ScreenTimeHours",
          "Value": "GT_Four",
          "CurrentHabit": "No",
          "HealthScore": 70.5364,
          "HealthGain": -0.6949
        },
        {
          "Variable": "MediterraneanDietAdherence",
          "Value": "Medium",
          "CurrentHabit": "No",
          "HealthScore": 70.3388,
          "HealthGain": -2.0415
        },
        {
          "Variable": "CoffeeConsumption",
          "Value": "LessOne",
          "CurrentHabit": "No",
          "HealthScore": 70.1179,
          "HealthGain": -3.5458
        },
        {
          "Variable": "ScreenTime",
          "Value": "One_Two",
          "CurrentHabit": "No",
          "HealthScore": 69.8544,
          "HealthGain": -5.3409
        },
        {
          "Variable": "JobStrain",
          "Value": "Yes",
          "CurrentHabit": "No",
          "HealthScore": 69.5737,
          "HealthGain": -7.2529
        },
        {
          "Variable": "AlcoholConsumption",
          "Value": "Heavy",
          "CurrentHabit": "No",
          "HealthScore": 69.5384,
          "HealthGain": -7.4934
        },
        {
          "Variable": "MediterraneanDietAdherence",
          "Value": "Low",
          "CurrentHabit": "No",
          "HealthScore": 69.4732,
          "HealthGain": -7.9376
        },
        {
          "Variable": "PhysicalActivity",
          "Value": "Moderately_Active",
          "CurrentHabit": "No",
          "HealthScore": 69.3969,
          "HealthGain": -8.4572
        },
        {
          "Variable": "CoffeeConsumption",
          "Value": "OneTwo",
          "CurrentHabit": "No",
          "HealthScore": 69.3639,
          "HealthGain": -8.6821
        },
        {
          "Variable": "ScreenTime",
          "Value": "Two_Three",
          "CurrentHabit": "No",
          "HealthScore": 69.2559,
          "HealthGain": -9.4177
        },
        {
          "Variable": "SystolicBP",
          "Value": "Mid_Low",
          "CurrentHabit": "No",
          "HealthScore": 68.5905,
          "HealthGain": -13.9502
        },
        {
          "Variable": "PhysicalActivity",
          "Value": "Inactive",
          "CurrentHabit": "No",
          "HealthScore": 67.2699,
          "HealthGain": -22.9452
        },
        {
          "Variable": "Obesity",
          "Value": "Obese",
          "CurrentHabit": "No",
          "HealthScore": 67.1449,
          "HealthGain": -23.7967
        },
        {
          "Variable": "ScreenTime",
          "Value": "Gt_three",
          "CurrentHabit": "No",
          "HealthScore": 66.7854,
          "HealthGain": -26.2457
        },
        {
          "Variable": "Smoking",
          "Value": "Former",
          "CurrentHabit": "No",
          "HealthScore": 66.2184,
          "HealthGain": -30.108
        },
        {
          "Variable": "SystolicBP",
          "Value": "Mid_High",
          "CurrentHabit": "No",
          "HealthScore": 63.8516,
          "HealthGain": -46.2293
        },
        {
          "Variable": "SystolicBP",
          "Value": "High",
          "CurrentHabit": "No",
          "HealthScore": 58.9364,
          "HealthGain": -79.7101
        },
        {
          "Variable": "Smoking",
          "Value": "Current",
          "CurrentHabit": "No",
          "HealthScore": 52.3861,
          "HealthGain": -124.3283
        }
      ]
    }
  }
}


function fetchToken() {
  axios.get(window.location.href + "middleware/auth_token").then(function (response) {
      // handle success
      
      sessionStorage.token = response.data.auth

    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })

}




function prepareDataForPost(data) {

  let template = {
    age: "25",
    sex: "Male",
    Weight: "80",
    Height: "170",
    Education: "High",
    FamilyHistoryCHD: "No",
    FamilyHistoryT2DM: "No",
    AlcoholConsumption: "Normal",
    Smoking: "Former",
    SleepDuration: "Long",
    JobStrain: "Yes",
    PhysicalActivity: "Inactive",
    MediterraneanDietAdherence: "Medium",
    CoffeeConsumption: "OneTwo",
    ScreenTimeHours: "ThreeFour",
    Obesity: "Obese",
    SystolicBP: "Mid_High",
    HDLCholesterol: "Medium"
  }

  for (var key in template) {
    if (data.hasOwnProperty(key)) {
      template[key] = data[key]
    }
  }

  return template

}

function postHealthData(data) {


  return new Promise((resolve, reject) => {

    let preparedData = prepareDataForPost(data)
    let token = sessionStorage.getItem('token')
    console.log(preparedData, sessionStorage.getItem('token'))
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }

    axios.post('https://diamonds.tno.nl/api/services/scripts/270', preparedData, {
        headers: headers
      })
      .then(response => {
        console.log(response.data)
        resolve(response.data)


      })
      .catch(error => {

        console.error(error)
        let data = returnDemoData()

        reject(error, data)


      });
    // // MAKE REQUEST
    // let data = returnDemoData()

    // if (data === null) {
    //     reject("api call failed")
    // }

    // setTimeout(() => {
    //     resolve(data)
    // }, 100)



  })







}