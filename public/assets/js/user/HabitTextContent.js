let lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris porttitor, enim sit amet efficitur efficitur, nulla dui imperdiet massa, id blandit mi orci ac metus. Sed lectus purus, dictum ac bibendum eu, viverra vulputate velit. Morbi blandit dapibus ante, sed maximus neque. Curabitur leo nibh, varius quis aliquam in, blandit sed eros. Pellentesque sit amet elementum nulla. Mauris eros nibh, venenatis rutrum euismod et, semper id massa. Praesent vel leo eget nisl sagittis aliquet sagittis ac lectus. Morbi dapibus convallis gravida. Donec quis suscipit neque. Ut vitae gravida risus. Praesent volutpat tempus ipsum et pharetra. Nam auctor justo quam, ut.'


const habits = [
     {
        name: "Job Strain",
        img: "assets/img/strain.png",
        mappings: habitMappings["JobStrain"],
        key: 'JobStrain',
        textBlurb: lorem,
        relatedDiseases: "CVD, COPD, Diabetes Type 2, Coronary Heart Disease, Stroke, Depression"
    },
     {
        name: "Smoking",
        img: "assets/img/smoking.png",
        mappings: habitMappings['Smoking'],
        key: 'Smoking',
        textBlurb: lorem,
        relatedDiseases: "CVD, COPD, Diabetes Type 2, Coronary Heart Disease, Stroke, Depression"

    },

     {
        name: "Alcohol Consumption",
        img: "assets/img/alcohol.png",
        mappings: habitMappings["AlcoholConsumption"],
        key: 'AlcoholConsumption',
        textBlurb: lorem,
        relatedDiseases: "CVD, COPD, Diabetes Type 2, Coronary Heart Disease, Stroke, Depression"

    },
     {
        name: "Coffee Consumption",
        img: "assets/img/coffee.png",
        mappings: habitMappings['CoffeeConsumption'],
        key: 'CoffeeConsumption',
        textBlurb: lorem,
        relatedDiseases: "CVD, COPD, Diabetes Type 2, Coronary Heart Disease, Stroke, Depression"
    },
     {
        name: "Sleeping Time",
        img: "assets/img/sleep.png",
        mappings: habitMappings['SleepDuration'],
        key: 'SleepDuration',
        textBlurb: lorem,
        relatedDiseases: "CVD, COPD, Diabetes Type 2, Coronary Heart Disease, Stroke, Depression"

    },
     {
        name: "Physical Activity",
        img: "assets/img/activity.png",
        mappings: habitMappings['PhysicalActivity'],
        key: 'PhysicalActivity',
        textBlurb: lorem,
        relatedDiseases: "CVD, COPD, Diabetes Type 2, Coronary Heart Disease, Stroke, Depression"

    },
     {
        name: "Screen Time",
        img: "assets/img/screen.png",
        mappings: habitMappings['ScreenTimeHours'],
        key: 'ScreenTime2',
        textBlurb: lorem,
        relatedDiseases: "CVD, COPD, Diabetes Type 2, Coronary Heart Disease, Stroke, Depression"

    },
     {
        name: "Mediterranean Diet",
        img: "assets/img/diet.png",
        mappings: habitMappings['MediterraneanDietAdherence'],
        key: 'MediterraneanDietAdherence',
        textBlurb: lorem,
        relatedDiseases: "CVD, COPD, Diabetes Type 2, Coronary Heart Disease, Stroke, Depression"

    },
     {
        name: "Weight",
        img: "assets/img/weight.png",
        mappings: habitMappings['Obesity'],
        key: 'Obesity',
        textBlurb: lorem,
        relatedDiseases: "CVD, COPD, Diabetes Type 2, Coronary Heart Disease, Stroke, Depression"

    },
     {
        name: "Blood Pressure",
        img: "assets/img/blood.png",
        mappings: habitMappings['SystolicBP'],
        key: 'SystolicBP',
        textBlurb: lorem,
        relatedDiseases: "CVD, COPD, Diabetes Type 2, Coronary Heart Disease, Stroke, Depression"

    },
    {
        name: "Cholesterol",
        img: "assets/img/heart.png",
        mappings: habitMappings['HDLCholesterol'],
        key: 'HDLCholesterol',
        textBlurb: lorem,
        relatedDiseases: "CVD, COPD, Diabetes Type 2, Coronary Heart Disease, Stroke, Depression"

    },

]