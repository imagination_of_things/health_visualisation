$(function () {

  console.log(JSON.parse(sessionStorage.getItem('userData')))
  let userData = JSON.parse(sessionStorage.getItem('userData'))



  let infoDrawerState = false
  let hasSentForData = false;
  let hasResizedFromNodeClick = false


  let infoSidebarState = false;
  let quizSidebarState = true;
  let habitLabelsState = true;

  //Canvas Logic -==-=-=-=------------------------------------------------------------------------------
  let container = document.getElementById('canvas-container')
  let two = new Two({
    width: container.offsetWidth,
    height: container.offsetHeight
  }).appendTo(container);
  two.scene.translation.set(two.width / 2, two.height / 2)
  let sceneManager = new SceneManager(two, nodeClickCallback);

  setTimeout(() => {
    sceneManager.setDisplayState(0)

  }, 500)

  // setTimeout(()=>{
  //   sceneManager.setDisplayState(1)
  //   toggleQuizView()
  // }, 5000  )

  // add noise layer
  canvasNoise(200, 200, window.devicePixelRatio, 0.008, 'noiseLayer');

  let handleResize = debounce(() => {

    sceneManager.treeLayer.opacity = 0
    sceneManager.leafLayer.opacity = 0

    gsap.to(sceneManager.treeLayer, 0.5, {
      opacity: 1,
      onUpdate: () => {
        sceneManager.leafLayer.opacity = sceneManager.treeLayer.opacity
      },
    })

    console.log("resize", sceneManager)
    two.width = container.offsetWidth
    two.height = container.offsetHeight
    two.scene.translation.set(two.width / 2, two.height / 2)

    let dimensions = {
      w: container.offsetWidth,
      h: container.offsetHeight
    }
    sceneManager.resize(dimensions)


  }, 750)















  function compare(a, b) {
    if (a.HealthScore[0] > b.HealthScore[0]) {
      return -1;
    }
    if (a.HealthScore[0] <= b.HealthScore[0]) {
      return 1;
    }
    return 0;
  }




  // JQuery Logic ---------------------------------------------------------------------------

  function setupUIListeners() {



    $("#submit-playground").click(() => {

      $('#butt-text').css('display', 'none')
      $('#loading-spinner').css('display', 'block')

      postHealthData(userData).then((healthScoreData) => {
        //Do Something with health data
        $('#alert').css('top', '-100px')

        $('#legend').css('opacity', '1')

        $('#legend').children().css('opacity', '1')


        hasSentForData = true
        healthScoreData[0].AllCases.sort(compare)
        console.log(healthScoreData[0].AllCases)

        // Sort recommendations


        toggleQuizView()
        setTimeout(() => {

          // Show Data
          sceneManager.setDisplayState(1)
          sceneManager.setData(userData, healthScoreData)

        }, 1500)
        $('#butt-text').css('display', 'block')
        $('#loading-spinner').css('display', 'none')
      }).catch((error, backupData) => {
        console.error(error)
        hasSentForData = true

        $('#alert').css('top', '0px')

        // toggleQuizView()
        // setTimeout(() => {

        //   // Show Data
        //   sceneManager.setDisplayState(1)
        //   sceneManager.setData(userData, backupData)

        // }, 1500)
        $('#butt-text').css('display', 'block')
        $('#loading-spinner').css('display', 'none')
      })

    })
    // All Buttons
    $('button').click(function () {
      console.log()
      let siblings = $(this).siblings()
      for (let i = 0; i < siblings.length; i++) {

        // console.log(siblings[i])
        $(siblings[i]).removeClass('active')
        // siblings[i].removeClass('active')


      }



      $(this).addClass('active')


      userData[$(this)[0].parentNode.id] = $(this)[0].value

    })


    // Weight
    $('#Weight').slider();
    $("#Weight").on("slide", function (slideEvt) {
      // $("#WeightSliderTooltip").text(slideEvt.value + "kg");
      userData.Weight = slideEvt.value

      console.log(userData.Weight, userData.Height)

      userData.BMI = calcBMI(userData.Height, userData.Weight)

      if (userData.BMI < 25) {
        userData.Obesity = "None"
      } else if (userData.BMI >= 25 && userData.BMI < 30) {
        userData.Obesity = "Overweight"

      } else {
        userData.Obesity = "Obese"

      }
      console.log(userData)

    });

    // BloodPressure

    $("#SystolicBP").on("slide", function (slideEvt) {
      // $("#WeightSliderTooltip").text(slideEvt.value + "kg");
      // console.log(habitMappings['SystolicBP'].levels[slideEvt.value-1])
      userData.SystolicBP = habitMappings['SystolicBP'].levels[slideEvt.value - 1]
      console.log(userData)

    });


    $("#CoffeeConsumption").on("slide", function (slideEvt) {
      // console.log(slideEvt.value)
      // console.log(habitMappings['CoffeeConsumption'].levels[slideEvt.value-1])

      userData.CoffeeConsumption = habitMappings['CoffeeConsumption'].levels[slideEvt.value - 1]

    });

    $("#ScreenTimeHours").on("slide", function (slideEvt) {
      // console.log(slideEvt.value)
      console.log(habitMappings['ScreenTimeHours'].levels[slideEvt.value - 1])

      userData.ScreenTimeHours = habitMappings['ScreenTimeHours'].levels[slideEvt.value - 1]

    });
  }


  function nodeClickCallback(nodeID, recommendationIndex, recommendations) {
    //Only act if quiz filled out
    if (hasSentForData) {


      // Get text

      let habit = habits.find(obj => obj.key == nodeID);
      console.log(habit)

      sceneManager.toggleHabitLabels(true)

      // Set drawer image text and diseases
      $('#desc-title').text(habit.name)

      $('#desc-paragraph').text(habit.textBlurb)
      $('#diseases').text(habit.relatedDiseases)
      $('#desc-image').attr("src", habit.img);

      // console.log(isARecommendation)
      if (recommendationIndex >= 0) {
        $('#recommendation').css('display', 'block')
        let rec = recommendations[recommendationIndex]
        let adviceIndex = habitMappings[rec.Variable[0]].levels.indexOf(rec.Value[0])
        let advice = habitMappings[rec.Variable[0]].aliases[adviceIndex] ? habitMappings[rec.Variable[0]].aliases[adviceIndex] : "None"
        console.log(adviceIndex, rec.Variable[0])
        let healthScoreGain = rec.HealthScore[0].toFixed(0)

        $('#improvement-badge').text(advice)
        $('#health-score-badge').text(healthScoreGain)


        console.log(rec, adviceIndex)
      } else {
        $('#recommendation').css('display', 'none')

      }

      openHabitInfoFromNodeClick()







    }
  }


  function toggleQuizView() {
    //If its opens close
    if (quizSidebarState) {
      // Close Quiz
      $('.sidebar-quiz').removeClass('quiz-open')
      $('#quiz-button').removeClass('open');
      $('#main-content').removeClass('main-small')
      $('.sidebar-info').removeClass('sidebar-hide')
      // if (infoDrawerState) {
      //   // Close Info
      //   toggleHabitInfo()
      // }
      quizSidebarState = false
      $('#legend').children().css('opacity', '1')

    } else if (!quizSidebarState) {
      // $('.sidebar-info').addClass('sidebar-hide')
      console.log(infoDrawerState)
      if (infoDrawerState) {
        // Close Info

      }
      $('.sidebar-info').removeClass('info-open')
      $('#info-button').removeClass('open');
      infoSidebarState = false

      if (hasSentForData) {

        $('#legend').children().css('opacity', '1')
      }
        sceneManager.toggleHabitLabels(false)

      


      $('.sidebar-quiz').addClass('quiz-open')
      $('#quiz-button').addClass('open');
      // make main small
      $('#main-content').addClass('main-small')
      $('.sidebar-info').addClass('sidebar-hide')

      $('#legend').children().css('opacity', '0')


      quizSidebarState = true
    }

    sceneManager.hideIndicator()

    handleResize()


  }

  function toggleHabitInfo() {

    if (infoSidebarState) {
      $('.sidebar-info').removeClass('info-open')
      $('#main-content').removeClass('main-small')
      $('#info-button').removeClass('open');
      infoSidebarState = false

      if (hasSentForData) {

        sceneManager.toggleHabitLabels(false)
        $('#legend').children().css('opacity', '1')
      }
      // if (quizSidebarState) {
      //   toggleQuizView()

      // }
    } else {


      // //If quiz is open close
      // if (quizSidebarState) {
      //   toggleQuizView()

      // }
      $('.sidebar-info').addClass('info-open')
      $('#main-content').addClass('main-small')
      $('#info-button').addClass('open');
      infoSidebarState = true

      if (hasSentForData) {

        sceneManager.toggleHabitLabels(true)
        $('#legend').children().css('opacity', '0')
      }

    }


    sceneManager.hideIndicator()

    // $('.sidebar-info').toggleClass('info-open')
    // $('#info-content').show()
    // $('#info-button').toggleClass('open');
    // sceneManager.toggleHabitLabels()
    // hasResizedFromNodeClick = false

    // $('#main-content').toggleClass('main-small')
    handleResize()

  }

  function openHabitInfoFromNodeClick() {
    $('.sidebar-info').addClass('info-open')
    $('#main-content').addClass('main-small')
    $('#info-button').addClass('open');

    if (!infoSidebarState) {
      handleResize()
      $('#legend').children().css('opacity', '0')

    }

    infoSidebarState = true


  }




  $('#info-button').click(() => {

    toggleHabitInfo()

    // if(infoDrawerState)


  })


  $('#back-to-start').click(() => {
    window.history.back();

    // window.location.href = // Put url to redirect
    // console.log(window.location.hostname + "/index.html")
    // if(infoDrawerState)


  })

  $('#quiz-button').click(() => {

    toggleQuizView()


  })


  setupUIListeners()



  // Resize event

  $(window).resize(handleResize);

  // Update event

  two
    .bind('update', (frameCount) => {
      // console.log(frameCount)
      sceneManager.update(frameCount)
    })
    .play();


});