function debounce(func, wait, immediate) {
	var timeout;
	return function () {
		var context = this,
			args = arguments;
		var later = function () {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};


const masterColors = {
    treeBlue: '#0976EF',
    treeWhiteAlpha: 'rgb(44,77,201)',
	bad: "rgba(223,101,0,0.7)",
	semibad: "rgba(236,166,50,0.7)",
	neutral: "rgba(250,232,101,0.7)",
	semigood: "rgba(160, 207, 27, 0.7)",
	good: "rgba(70,183,86,0.7)",
	
    badsolid: "rgba(223,101,0,0.97)",
    neutralsolid: "rgba(250,232,101,0.97)",
    goodsolid: "rgba(70,183,71,0.97)",
    background: "rgb(23, 40, 70)"
}

function lerp(x1, x2, t) {
    return x1 + t * (x2 - x1)
}


function sortDataByBiggestGain(healthData) {

	// console.log

}

const TWOPI = Math.PI * 2

const habitMappings = {
	"ScreenTime2": {
		levels: [
			"Less_one", "One_Two", "Two_Three", "Three_Four", "GT_Four", "LT_two"
		],
		aliases: [
			"<1hr", "1-2hrs", "2-3hrs", "3-4hrs", ">4hrs", "<2hrs"
		],
		colors: [
			masterColors.good, masterColors.semigood, masterColors.neutral, masterColors.semibad, masterColors.bad
		], 

		branchIndex: 6
	},

	"ScreenTime": {
		levels: [
			"Less_one", "One_Two", "Two_Three", "Three_Four", "GT_Four"
		],
		aliases: [
			"<1hr", "1-2hrs", "2-3hrs", "3-4hrs", ">4hrs", "<2hrs"
		],
		colors: [
			masterColors.good, masterColors.semigood, masterColors.neutral, masterColors.semibad, masterColors.bad
		], 

		branchIndex: 6
	},
	"ScreenTimeHours": {
		levels: [
			"Less_one", "One_Two", "Two_Three", "Three_Four", "GT_Four", "LT_two"
		],

		aliases: [
			"<1hr", "1-2hrs", "2-3hrs", "3-4hrs", ">4hrs", "<2hrs"
		],
		colors: [
			masterColors.good, masterColors.good, masterColors.neutral, masterColors.bad, masterColors.bad
		], 

		branchIndex: 6
	},
	"SystolicBP": {
		levels: [
			"Low", "Mid_Low", "Mid_High", "High"

		],
		aliases: [
			"Low", "Mid_Low", "Mid_High", "High"

		],
		colors: [
			masterColors.good, masterColors.semigood, masterColors.neutral, masterColors.bad

		], 
		branchIndex: 9
	},
	"CoffeeConsumption": {
		levels: [
			"LessOne", "OneTwo", "TwoThree", "ThreeFour", "FourMore"

		],
		aliases: [
			"<1/day", "1-2/day", "2-3/day", "3-4/day", ">4/day"

		],

		colors: [
			masterColors.good, masterColors.semigood, masterColors.neutral, masterColors.semibad, masterColors.bad
		], 
		branchIndex: 3, 


	},
	"Education": {
		levels: [
			"Low",
			"Intermediate", "High"
		],

		colors: [
			masterColors.bad, masterColors.neutral, masterColors.good
		],

		branchIndex: -1
		
	},
	"Smoking": {
		levels: [
			"Never", "Former", "Current"

		],
		aliases: [
			"None", "None", "None"
		],
		colors: [
			masterColors.good, masterColors.neutral, masterColors.bad
		], 
		branchIndex: 1,
	},
	"AlcoholConsumption": {
		levels: [
			"None", "Normal", "Heavy"
		],

		aliases: [
			"None", "Normal", "Heavy"
		],
		colors: [
			masterColors.good, masterColors.neutral, masterColors.bad
		], 
		branchIndex: 2
	},
	"SleepDuration": {
		levels: [
			"Short", "Normal", "Long"
		],

		aliases: [
			"Short", "Normal", "Long"

		],
		colors: [
			masterColors.bad, masterColors.neutral, masterColors.good
		], 
		branchIndex: 4
	},
	"JobStrain": {
		levels: [
			"No", "Yes"

		],
		aliases: [
			"No", "Yes"

		],
		colors: [
			masterColors.good, masterColors.bad
		], 
		branchIndex: 0
	},
	"MediterraneanDietAdherence": {
		levels: [
			"Low", "Medium", "High"
		],
		aliases: [
			"Low", "Medium", "High"
		],
		colors: [
			masterColors.bad, masterColors.neutral, masterColors.good
		], 

		branchIndex: 7
	},
	"Obesity": {
		levels: [
			"None", "Overweight", "Obese"
		],

		aliases: [
			"None", "Overweight", "Obese"
		],
		colors: [
			masterColors.good, masterColors.neutral, masterColors.bad
		], 

		branchIndex: 8
	},
	"HDLCholesterol": {
		levels: [
			"Low",  "Medium", "High"  
		],

		aliases: [
			"Low",  "Medium", "High"  
		],
		colors: [
			masterColors.good, masterColors.neutral, masterColors.bad
		], 
		branchIndex: 10
	},
	"PhysicalActivity": {
		levels: [
			"Active", "Moderately_Active", "Inactive"
		],
		aliases: [
			"Active", "Moderately_Active", "Inactive"
		],
		colors: [
			masterColors.good, masterColors.neutral, masterColors.bad
		], 
		branchIndex: 5
	},




}



function calcBMI(height, weight) {
	console.log(height, weight)
	height = height / 100
	return weight / (height * height)
}



