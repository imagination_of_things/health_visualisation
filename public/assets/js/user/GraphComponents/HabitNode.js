class HabitNode {
    constructor(position, color, group, two, index) {
        this.position = position;
        this.two = two;
        this.color = color;
        this.group = group;
        this.drawingGroup = new Two.Group()
        this.group.add(this.drawingGroup)

        this.size = this.two.width > this.two.height ? 10 + this.two.width / 100 : 10 + this.two.height / 100;
        this.index = index
        this.random = Math.random()
        this.key = habits[this.index].key

        this.generateShape()
        this.text = this.generateText()

        this.textState = false


        console.log(this.key)

        this.isRecommendation = false
        this.colorMappings = habits[10 - this.index].mappings




        // console.log(this.position)

    }

    setData(colorIndex, recommendation) {
        console.log(recommendation)
        this.isRecommendation = false;
        if (colorIndex === 0) {
            let index = this.colorMappings.levels.indexOf(recommendation)

            this.color = this.colorMappings.colors[index] ? this.colorMappings.colors[index] : masterColors.neutral

            this.isRecommendation = true

        } else if (colorIndex === 1) {
            let index = this.colorMappings.levels.indexOf(recommendation)

            this.color = this.colorMappings.colors[index] ? this.colorMappings.colors[index] : masterColors.neutral

            this.isRecommendation = true

        } else if (colorIndex === 2) {
            let index = this.colorMappings.levels.indexOf(recommendation)

            this.color = this.colorMappings.colors[index] ? this.colorMappings.colors[index] : masterColors.neutral

            this.isRecommendation = true

        } else {
            this.color = masterColors.treeBlue

        }

        this.circle.fill = this.color

        // gsap.to(this.circle, 0.5, {
        //     fill: this.color
        // })

    }

    // setIndex(index){
    //     this.index = index
    // }

    generateText() {
        let textPos = new Two.Vector(0, 0)
        let alignment = "middle"
        if (this.index < 5) {
            textPos.x = this.position.x - 40
            textPos.y = this.position.y
            alignment = "right"
        } else if (this.index === 5) {
            textPos.x = this.position.x
            textPos.y = this.position.y - this.two.height / 16
            alignment = "middle"

        } else if (this.index > 5) {
            textPos.x = this.position.x + 40
            textPos.y = this.position.y
            alignment = "left"

        }
        let text = new Two.Text(habits[this.index].name, textPos.x, textPos.y, {
            size: 10 + this.two.width / 200
        })
        text.fill = "white"
        text.alignment = alignment
        text.opacity = 0
        this.drawingGroup.add(text)

        return text;
    }
    // Here is where we first draw the shape whatever it is
    generateShape() {
        this.circle = new Two.Ellipse(this.position.x, this.position.y, this.size);
        this.circle.fill = this.color;
        this.circle.noStroke();

        this.sprite = new Two.Sprite(habits[this.index].img, this.position.x, this.position.y, 1, 1)
        // sprite.size = this.size/2
        this.sprite.scale = 0.1

        this.circle.opacity = 0

        this.sprite.opacity = 0

        // this.sprite.id = "sprite"+this.index
        this.overlayCircle = new Two.Ellipse(this.position.x, this.position.y, this.size);
        this.overlayCircle.fill = 'transparent';
        this.overlayCircle.noStroke();
        this.overlayCircle.id = `${habits[this.index].key}`
        // console.log(t)
        // this.overlayCircle.noStroke();

        this.group.add(this.circle)
        this.group.add(this.sprite)

        this.group.add(this.overlayCircle)

    }


    update(t) {
        // this.circle.position.y +=  Math.sin(Math.sin(t*0.001*this.index*this.random))*0.06
        // this.sprite.position.y +=  Math.sin(Math.sin(t*0.001*this.index*this.random))*0.06




    }

    hideShowHabitNodes(bool) {

        if (!bool) {
            gsap.to(this.text, 0.25, {
                opacity: 0,
                delay: 0.55 - this.index * 0.05,
                onUpdate: () => {
                    this.circle.opacity = this.text.opacity
                    this.sprite.opacity = this.text.opacity

                },

            })
        } else {
            gsap.to(this.text, 0.25, {
                opacity: 1,
                delay: this.index * 0.05,
                onUpdate: () => {
                    this.circle.opacity = this.text.opacity
                    this.sprite.opacity = this.text.opacity

                },

            })
        }




    }

    toggleLabels(bool) {

        if (bool) {
            gsap.to(this.text, 0.25, {
                opacity: 0,
                delay: 0.55 - this.index * 0.05,

                onComplete: () => {
                    this.textState = false
                }
            })
        } else {

                gsap.to(this.text, 0.25, {
                    opacity: 1,
                    delay: this.index * 0.05,
                    onComplete: () => {
                        this.textState = true
                    }
                })
            
        }

    }

    resize(newPosition) {
        this.position = newPosition
        this.size = this.two.width > this.two.height ? 10 + this.two.width / 100 : 10 + this.two.height / 100;

        this.circle.position.x = newPosition.x
        this.circle.position.y = newPosition.y
        this.circle.size = this.size
        this.sprite.position.x = newPosition.x
        this.sprite.position.y = newPosition.y


        this.overlayCircle.position.x = newPosition.x
        this.overlayCircle.position.y = newPosition.y
        this.overlayCircle.size = this.size
        let textPos = new Two.Vector(0, 0)
        let alignment = "middle"
        if (this.index < 5) {
            textPos.x = this.position.x - 40
            textPos.y = this.position.y
            alignment = "right"
        } else if (this.index === 5) {
            let y = this.two.height / 17 < 40 ? this.two.height / 17 : 40
            textPos.x = this.position.x
            textPos.y = this.position.y - y
            alignment = "middle"

        } else if (this.index > 5) {
            textPos.x = this.position.x + 40
            textPos.y = this.position.y
            alignment = "left"

        }

        this.text.position.x = textPos.x
        this.text.position.y = textPos.y

    }


    // Events
    handleClick() {
        //console.log(this.path.id)
    }
}