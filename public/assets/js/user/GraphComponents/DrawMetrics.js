function calculateTreeDrawMetrics(two) {
    this.two = two;
    this.drawData = {
        branches: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
        rulerPoints: [], 
        branchLength:  0,
        lineWidth: 5, 
        height: 0

    }

    this.drawData.height =  this.two.height * 0.25
    this.drawData.branchLength =  this.two.width * 0.2 < 250? this.two.width * 0.2 : 250;

    // Calculate branching positions we go in a mirrorlike fashion

    for (let i = 0; i < 5; i++) {

        let branchAngle = Math.PI / 2.34 - i * Math.PI / 13


        // Set angles 
        this.drawData.branches[i].theta = branchAngle
        this.drawData.branches[10 - i].theta = branchAngle

        // Branch origin
        this.drawData.branches[i].y1 = 2 * this.drawData.height / 8 - ((i + 1) * (this.drawData.height / 8))
        this.drawData.branches[i].x1 = 0
        this.drawData.branches[10 - i].y1 = 2 * this.drawData.height / 8 - ((i + 1) * (this.drawData.height / 8))
        this.drawData.branches[10 - i].x1 = 0
        // Branch End
        this.drawData.branches[i].y2 = this.drawData.branches[i].y1 - this.drawData.branchLength * Math.cos(branchAngle)
        this.drawData.branches[i].x2 = this.drawData.branchLength * Math.sin(branchAngle)
        this.drawData.branches[10 - i].y2 = this.drawData.branches[10 - i].y1 - this.drawData.branchLength * Math.cos(branchAngle)
        this.drawData.branches[10 - i].x2 = -this.drawData.branchLength * Math.sin(branchAngle)


        // Cater for middle node

        if (i === 4) {
            this.drawData.branches[5].theta = 2 * Math.PI

            this.drawData.branches[5].y1 = -this.drawData.height / 2.5

            this.drawData.branches[5].x1 = 0
            this.drawData.branches[5].y2 = this.drawData.branches[6].y1 - this.drawData.branchLength
            this.drawData.branches[5].x2 = 0
        }



        // this.two.makeCircle(0,y1,10)

    }

    // These are the points on the tree where leaf nodes connect

    for (let i = 0; i < 8; i++) {
        this.drawData.rulerPoints.push({
            y: 4 * this.drawData.height / 8 - ((i + 1) * (this.drawData.height / 8))
        })

    }




    return this.drawData
}