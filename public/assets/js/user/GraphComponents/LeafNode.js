const rulerTable = {
    0: 2,
    1: 3,
    2: 4,
    3: 5,
    4: 6, 
    5: 7,
    6: 6, 
    7: 5, 
    8: 4, 
    9: 3, 
    10:2 
}

class LeafNode {
    constructor(drawData, level, color, two, group, index){
        this.level = level;
        this.color = color
        this.two = two;
        this.index = index
        this.group = group;
        this.drawingGroup = new Two.Group()
        this.group.add(this.drawingGroup)
        if(this.index<5){
            this.direction = 1
        } else {
            this.direction = -1
        }
        this.displayState = -1
        this.tweening = false

   

        this.drawData = drawData



        this.drawLeaf();
        
        this.key = habits[10-this.index].key
        this.colorMappings = habits[10-this.index].mappings
        console.log(this.colorMappings, 10-this.index)


    }


    setDisplayState(dS) {
        this.displayState = dS
        if(dS === 0){
            // put in entry mode
            this.color = masterColors.good
            this.displayState = 0
            this.tweenLeaf(0.7, true)

            // setTimeout()
        } else if(dS === 1) {
            // data mode
            this.displayState = 1

            this.color = masterColors.bad

            this.tweenLeaf(0.27, false)
        }
    }

    drawLeaf() {

  

        // Position along y axis
        this.position = new Two.Vector(0, this.drawData.rulerPoints[rulerTable[this.index]].y);

        let circle = new Two.Rectangle(0,0, 3*this.two.height/9,  3*this.two.height/9);
        this.path = new Two.Path([circle.vertices[0],circle.vertices[1], circle.vertices[2]], false, true)
        // this.path.clip  
        this.path.join = 'miter'
        let above = -this.two.height/10
        let below = this.two.height/20


        // Deal with aesthetic 2,1
        if(rulerTable[this.index] >=6 ){
            above = -this.two.height/20
            below = this.two.height/20

        }
       
        this.path.vertices[0].x = this.position.x
        this.path.vertices[0].y = this.position.y+below
        this.path.vertices[1].x = lerp(this.position.x, this.drawData.branches[this.index].x2,this.level)
        this.path.vertices[1].y = lerp(this.position.y, this.drawData.branches[this.index].y2, this.level)
        this.path.vertices[2].x = this.position.x
        this.path.vertices[2].y = this.position.y+above
        // this.path.vertices[3].x = this.position.x
        // this.path.vertices[3].y = this.position.y
        // this.path.vertices[3].command = "Z"

        if(this.index === 5){
            let circle = new Two.Rectangle(this.position.x, this.position.y, this.two.height/32,  this.two.height/8);

            this.path = new Two.Path(circle.vertices, false, true)
    
            this.path.vertices[0].x =this.position.x+20
            this.path.vertices[0].y =this.position.y
            this.path.vertices[1].x = this.position.x
            this.path.vertices[1].y =lerp(this.position.y, this.drawData.branches[this.index].y2, this.level)
            this.path.vertices[2].x =this.position.x-20
            this.path.vertices[2].y =this.position.y
                 this.path.vertices[3].x = this.position.x
        this.path.vertices[3].y = this.position.y+100
            // this.path.vertices[2].command = "Z"

        }

        // this.path.vertices[1].y = 0 
        // this.two.makeCircle(this.path.vertices[1].x, this.path.vertices[1].y,2)

        
        // this.path.stroke = "rgb(255,255,0)"this.
        this.path.noStroke();
        this.path.fill = this.color
        // this.path.fill = "transparent"
        // this.path.stroke = this.color
        // this.path.automatic = true


        //console.log(this.path)
        this.drawingGroup.add(this.path)



    }

    setData(label, level, first) {

        if(!first) {
            let index = this.colorMappings.levels.indexOf(label)
            this.color = this.colorMappings.colors[index]?this.colorMappings.colors[index]:masterColors.neutral
            

            

        }
        
     

        this.tweenLeaf(level, first)

        
    }
    tweenLeaf(level, first) {

        this.tweening = true

        gsap.to(this.path, 0.5, {
            fill: this.color
        })

        if(this.index !=5){
            let objx = {value: lerp(this.position.x, this.drawData.branches[this.index].x2,this.level)}
            let objy = {value: lerp(this.position.y, this.drawData.branches[this.index].y2,this.level)}

            gsap.to(objx, 0.5, {
                value: lerp(this.position.x, this.drawData.branches[this.index].x2,level),
                onUpdate: ()=>{
                  this.path.vertices[1].x = objx.value
                },

                onComplete: ()=>{
                    this.level = level
                    this.tweening = false

                    if(first){
                        this.displayState = 0
                    }

                }
            })

            gsap.to(objy, 0.5, {
                value: lerp(this.position.y, this.drawData.branches[this.index].y2,level),
                onUpdate: ()=>{
                  this.path.vertices[1].y = objy.value
                },

                onComplete: ()=>{
                    this.level = level

                }
            })
        } else {
            let objy = {value: lerp(this.position.y, this.drawData.branches[this.index].y2,this.level)}

            gsap.to(objy, 0.5, {
                value: lerp(this.position.y, this.drawData.branches[this.index].y2,level),
                onUpdate: ()=>{
                  this.path.vertices[1].y = objy.value
                },

                onComplete: ()=>{
                    this.level = level
                    this.tweening = false
                    if(first){
                        this.displayState = 0
                    }

                }
            })
        }
    }

    resize(newDrawingData){

        this.drawData = newDrawingData

        this.drawingGroup.remove(this.path)
        this.drawLeaf()




    }

    update(t, frameCount) {


        //Breathing

        
            if(this.displayState === 0 && !this.tweening){
                let objx = {value: lerp(this.position.x, this.drawData.branches[this.index].x2,this.level-0.05*Math.sin(t*0.001))}
                let objy = {value: lerp(this.position.y, this.drawData.branches[this.index].y2,this.level-0.05*Math.sin(t*0.001))}
                this.path.vertices[1].x = objx.value
                this.path.vertices[1].y = objy.value

            } else {

            }
      

    }
}