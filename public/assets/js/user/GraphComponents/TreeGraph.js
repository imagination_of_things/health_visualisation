class TreeGraph {
    constructor(group, two, color) {
        this.two = two;
        this.color = color;
        this.white = 'rgba(255,255,255,0.1)'
        this.group = group;
        this.drawingGroup = new Two.Group()

        this.drawData = calculateTreeDrawMetrics(this.two)

        // Data for branch length

        this.numericData = []
        for(let i=0; i<11;i++){
            this.numericData.push(0)
        }

 
        this.displayState = -1
     


    }


    setDisplayState(dS) {
        this.displayState = dS

        if(this.displayState === 0){
            for(let i=0; i<11;i++){
                this.numericData[i] = 0.75
            }
            this.updateOverlays()
        }
    }



    display() {
        this.group.add(this.drawingGroup)

        let groupArray = this.drawBranches()
        this.branches = groupArray[0]
        this.overlays = groupArray[1]
        

        this.overlayPositions = []

        this.trunk = this.drawTrunk()
        this.drawingGroup.add(this.trunk)

        this.drawingGroup.add(this.branches)
        this.drawingGroup.add(this.overlays)


    }


    setBranchData(data) {

        console.log(data)


        for(let i=0; i<data.length;i++){
            let maxHealthScore = data[i]['BranchLevel'] > 100 ? 100 : data[i]['BranchLevel']
            let index = data[i]["branchIndex"]

            if(index >= 0 && data[i]["Variable"]!="ScreenTime"){
                this.numericData[10-index] = maxHealthScore/100

            }


            
        }


        this.updateOverlays()

       
    }






    drawTrunk() {

        // generate vertices
        // four blr tlr
        let trunk = new Two.Rectangle(0, 0, this.drawData.lineWidth, this.two.height/2)

        trunk.fill = this.color;
        trunk.noStroke();
        trunk.visible = false;
        let points = []

        trunk.vertices.forEach((vertex, index) => {
            if (index === 2) {
                points.push(vertex.x + 5, vertex.y)

            } else if (index === 3) {

                points.push(vertex.x - 5, vertex.y)

            } else {
                points.push(vertex.x, vertex.y)

            }

        });
        let path = this.two.makePath(points[0], points[1], points[2], points[3], points[4], points[5], points[6], points[7], points[8], true)
        path.fill = this.color
        path.noStroke();
        path.translation.y = this.two.height/8



        // trunk.vertices[0].x = 20

        // t.add(trunk)


        return path
    }

    drawBranches() {
        let half = Math.floor(11 / 2)
        let branches = new Two.Group()
        let overlayBranches = new Two.Group()
        let x1 = 0


        for(let i=0; i<this.drawData.branches.length; i++) {
            let branchData = this.drawData.branches[i]

            let branch = new Two.Line(branchData.x1, branchData.y1, branchData.x2, branchData.y2)
            branch.stroke = this.white
            branch.linewidth = this.drawData.lineWidth
            branches.add(branch)

            // Overlay
            let overlay =  new Two.Line(branchData.x1, branchData.y1, lerp(branchData.x1,branchData.x2,this.numericData[i]), lerp(branchData.y1,branchData.y2, this.numericData[i]))
            overlay.stroke = this.color
            overlay.linewidth = this.drawData.lineWidth
            overlayBranches.add(overlay)

            // this.overlayPositions.add(overlay.vertices)

        }
        return [branches,overlayBranches]
    }


    updateOverlays() {

        for(let i=0; i<11;i++){
            let obj = {value:0}
            gsap.to(obj, 1.5, {
                value: this.numericData[i],
                onUpdate: ()=>{
                    this.overlays.children[i].vertices[1].x = lerp(this.overlays.children[i].vertices[0].x,this.branches.children[i].vertices[1].x,obj.value)
                    this.overlays.children[i].vertices[1].y = lerp(this.overlays.children[i].vertices[0].y,this.branches.children[i].vertices[1].y,obj.value)
                },

               
            })
        

        }
    }


    update(t, frameCount) {



       




    }

    resize() {
        // this.trunk.position.x = this.two.width
        // this.trunk.
        this.drawData = calculateTreeDrawMetrics(this.two);
        // this.drawingGroup.remove(this.trunk)
        this.drawingGroup.remove(this.overlays)
        this.drawingGroup.remove(this.branches)
        this.drawingGroup.remove(this.trunk)

       
        this.display()


    


    }


    // Events
    handleClick() {}
}