class TextNode {
    constructor(index, two, group) {
        this.state = 4;
        this.index = index;
        this.two = two;
        this.position = new Two.Vector(0, 0);
        this.color = masterColors.good;
        this.group = group;
        this.textGroup = new Two.Group()

        this.displayState = 0

        this.group.add(this.textGroup)
        this.baseSize = 0;
        this.height = 100;
        this.data = 20

        this.gender = "Males"
        this.text = this.getLabel()
        // this.textGroup.opacity = 0




        // this.index = 1
        this.random = Math.random()
        // this.textGroup.position.y = this.two.height/2 -this.two.height/8
        this.generateShape()
        this.setState(4)

        this.setDisplayState(0)



    }



    setData(number, gender) {

        this.data = number

        this.scoreView.value = this.data

        console.log(number)
     

        if(gender) {
            this.gender = gender
            if(this.index === 3){
                console.log(gender)

                this.labelView.value = gender

            }

        }
    }


    setDisplayState(dS) {
        this.displayState = dS

        if (this.displayState === 0) {
            gsap.to(this.scoreView, 0.5, {
                opacity: 0,
                onUpdate: () => {
                    this.labelView.opacity = this.scoreView.opacity
                    this.sublabelView.opacity = this.scoreView.opacity*0.5

                }
            })

        } else if (this.displayState === 1) {
            gsap.to(this.scoreView, 0.5, {
                opacity: 1,
                onUpdate: () => {
                    this.labelView.opacity = this.scoreView.opacity
                    this.sublabelView.opacity = this.scoreView.opacity*0.5

                }
            })

        }
    }



    setState(state) {
        this.state = state

        // if(state === 4 && this.index === 3 ){
        //     gsap.to(this.scoreView, 0.5, {
        //         opacity: 1, 
        //         onUpdate: ()=>{
        //             this.labelView.opacity = this.scoreView.opacity
        //         }
        //     })

        //     // this.textGroup.opacity = 1



        // } else if(state === 3 && this.index === 3) {
        //     gsap.to(this.scoreView, 0.5, {
        //         opacity: 0,
        //         onUpdate: ()=>{
        //             this.labelView.opacity = this.scoreView.opacity

        //         }
        //     })         
        //     // this.textGroup.opacity = 0



        // }

        this.updateLayout(state)

    }



    updateLayout() {

        this.calculatePosition()




    }

    calculatePosition() {

        // if (this.state === 4) {

            this.baseSize = 2 * this.two.width / 13
            let rulerPoint = 2 + this.index * 3
            this.position = new Two.Vector(-this.two.width / 2 + rulerPoint * this.baseSize / 2, this.two.height / 2)
        // } else if (this.state === 3) {
        //     this.baseSize = 2 * this.two.width / 13
        //     let rulerPoint = 3.5 + this.index * 3
        //     this.position = new Two.Vector(-this.two.width / 2 + rulerPoint * this.baseSize / 2, this.two.height / 2)


        // }

        let newPos = {
            x: this.position.x
        }

        gsap.to(this.textGroup.position, 0.5, {
            x: newPos.x - this.baseSize / 2
        })


        this.textGroup.position.y = this.two.height / 2 - this.two.height / 16


    }

    getLabel() {
        let label = ''
        let sublabel = ''
        switch (this.index) {
            case 0:

                label = "Your Health Score"
                break;

            case 1:

                label = "Globe"
                break;

            case 2:

                label = "Your Age Group"
                break;

            case 3:

                label = this.gender
                break;

            default:
                break;
        }

        return label
    }




    // Here is where we first draw the shape whatever it is
    generateShape(redraw) {
        this.calculatePosition()

        this.scoreView = this.two.makeText(`${this.data}`, this.baseSize / 2, -20)
        this.scoreView.fill = "white"
        this.scoreView.size = 18 + this.two.width / 120
        // this.scoreView.weight = 900


        if(this.index === 0){
            this.labelView = this.two.makeText(`${this.text}`, this.baseSize / 2, -20 + this.scoreView.size)
            this.labelView.fill = "white"
            this.labelView.opacity = 0.8
            this.labelView.size = 9 + this.two.width / 120
            this.labelView.weight = 'lighter'

            this.sublabelView = this.two.makeText(``, this.baseSize / 2, -20 + this.scoreView.size)
            this.sublabelView.fill = "transparent"
            this.sublabelView.opacity = 0.8
            this.sublabelView.size = 9 + this.two.width / 100
            this.sublabelView.weight = 'lighter'
    
        } else {
            this.labelView = this.two.makeText(`${this.text}`, this.baseSize / 2, 0 + this.scoreView.size )
            this.labelView.fill = "white"
            this.labelView.opacity = 0.8
            this.labelView.size = 9 + this.two.width / 120
            this.labelView.weight = 'lighter'
            this.sublabelView = this.two.makeText(`Health Score`, this.baseSize / 2, -20 + this.scoreView.size -this.scoreView.size*0.1)
            this.sublabelView.fill = "white"
            this.sublabelView.opacity = 0.5
            this.sublabelView.size = 6 + this.two.width / 120
            this.sublabelView.weight = 'lighter'
        }


        this.textGroup.add(this.scoreView)
        this.textGroup.add(this.labelView)
        this.textGroup.add(this.sublabelView)



        //Default invisible

        if(this.displayState === 0){
            this.scoreView.opacity = 0
            this.labelView.opacity = 0
            this.sublabelView.opacity = 0
        }
       

        // return null
    }


    redrawText() {
        this.textGroup.remove(this.scoreView)
        this.textGroup.remove(this.labelView)
        this.textGroup.remove(this.sublabelView)
       
        this.generateShape()

    }



    demoAnim(fC) {


    }


    update(t, fC) {
        // this.path.position.y +=  Math.sin(Math.sin(t*0.005*this.index*this.random))*0.06




    }

    resize() {

        // if(this.state)
        this.redrawText()
        this.setState(4)




    }


    // Events
    handleClick() {
        //console.log(this.path.id)
    }
}