class MountainNode {
    constructor(index, two, group) {
        this.state = 4;
        this.displayState = 0;
        this.index = index;
        this.two = two;
        this.position = new Two.Vector(0, 0);
        this.color = masterColors.good;
        this.group = group;
        this.data = 0.001
        this.baseShowing = false;

        this.drawingGroup = new Two.Group()
        this.group.add(this.drawingGroup)
        this.shapeGroup = new Two.Group()

        this.indicatorGroup = new Two.Group()

        this.drawingGroup.add(this.shapeGroup)
        this.drawingGroup.add(this.indicatorGroup)

        this.baseSize = 0;
        this.height = 0;
        this.base = this.generateShape();
        this.base.fill = masterColors.good;
        this.base.opacity = 0;
        this.path = this.generateShape()
        
        // this.index = 1
        this.random = Math.random()
        this.shapeGroup.position.y = this.two.height/2 -this.two.height/8

        

        this.line = new Two.Line(-this.two.width/2,0, this.two.width,0)
        this.line.stroke = "rgba(255,255,255,0.3)"
        this.line.opacity = 0
       
        this.line.position.y = 0.5*this.two.height - 0.125*this.two.height - this.data*this.two.height/4
        this.drawingGroup.add(this.line)
        
        // if (this.state === 3 && this.index === 3) {
        //     this.shapeGroup.opacity = 0
        // }
        
        let rect = new Two.Rectangle(this.baseSize,0,40,20)
        this.indicator = new Two.Path(rect.vertices, true, true)
        this.indicator.fill = "black"

        this.text = new Two.Text("00", 0, 2, {fill:"white", size: 18+this.two.width/150});
        this.indicatorGroup.add(this.indicator)
        this.indicatorGroup.add(this.text)
        this.indicatorGroup.opacity = 0
  


        ////console.log(this.position)
       

    }


    setDisplayState(dS) {
        this.displayState = dS

        if(dS === 0){
            this.setData(0.7)
            // this.toggleMountain(true)
        } else if (dS === 1) {

        }
    }

   

    setState(state) {
        this.state = state

       

        this.updateLayout(state)

    }


    toggleMountain(bool){

        if(!bool){
            gsap.to(this.drawingGroup, 0.25, {
                opacity: 0,
                delay: 0.55-this.index*0.05,
                onComplete: ()=>{
                }
            })
        } else {
            gsap.to(this.drawingGroup, 0.25, {
                opacity: 1, 
                delay: this.index*0.05,

                onComplete: ()=>{

                }
            })
        }


    }



    updateLayout(state) {

        this.calculatePosition()




    }

    calculatePosition() {
        ////console.log("width", this.two.width)

        if (this.state === 3) {

            this.baseSize = 2 * this.two.width / 13
            let rulerPoint = 2 + this.index * 3
            this.position = new Two.Vector(-this.two.width / 2 + rulerPoint * this.baseSize / 2, this.two.height / 2)
        } else if(this.state === 4) {
            this.baseSize = 2 * this.two.width / 13
            let rulerPoint = 3.5 + this.index * 3
            this.position = new Two.Vector(-this.two.width / 2 + rulerPoint * this.baseSize / 2, this.two.height / 2)


        }

        let newPos = {x: this.position.x-this.baseSize}

        gsap.to(this.shapeGroup.position, 0.5, {
            x: newPos.x
        })
        gsap.to(this.indicatorGroup.position, 0.5, {
            x: newPos.x+this.baseSize
        })

        this.shapeGroup.position.y = this.two.height/2-this.two.height/8
        this.indicatorGroup.position.y = this.two.height/2-this.two.height/8

        // this.setData()
    }

  


    // Here is where we first draw the shape whatever it is
    generateShape() {
        this.calculatePosition()


        // this.two.makeCircle(this.position.x, this.position.y, 20)

        let trianglePositions = [
            new Two.Anchor(0, 0),
            new Two.Anchor(this.baseSize, - this.height),
            new Two.Anchor(this.baseSize*2, 0)
        ]


        ////console.log(trianglePositions)
        let triangle = this.two.makePath(trianglePositions, false, true);
        triangle.fill = this.color;
        triangle.noStroke();

        this.shapeGroup.add(triangle)




        return triangle
    }

    redrawTriangles() {
        // this.baseSize = 2 * this.two.width / 13
        // this.height = 

        // this.shapeGroup.scale = 0.8
        this.shapeGroup.remove(this.path)

        this.shapeGroup.remove(this.base)
        this.base = this.generateShape();
        this.base.fill = masterColors.good;

        // if(!this.baseShowing){
        //     this.base.opacity = 0;

        // }
        this.path = this.generateShape()
    }

    redrawLine() {
        this.drawingGroup.remove(this.line)
        let linepos = 0.5*this.two.height - 0.125*this.two.height - this.data*this.two.height/4

        this.line = new Two.Line(-this.two.width/2, 0, this.two.width, 0)
        this.line.stroke = "rgba(255,255,255,0.3)"
        this.line.opacity = 0
        if(this.index === 0){
            this.line.opacity =1
        }

        this.line.position.y = linepos

        this.drawingGroup.add(this.line)
    }

    setData(data) {

     

      

        this.data = data

        if(this.data<0.3){
            this.color = masterColors.bad;
        } else if(this.data>=0.3 && this.data<0.7){
            this.color = masterColors.neutral
        } else {
            this.color = masterColors.good
        }

        gsap.to(this.path, 1.0, {
            fill: this.color
        })

     

        ////console.log(this.data)
        let value = -this.data*this.two.height/4
        let linepos = 0.5*this.two.height - 0.125*this.two.height - this.data*this.two.height/4
        

        gsap.to(this.path.vertices[1], 0.5, {
            y: value, 
            delay: this.index*0.1,
        })
        


        gsap.to(this.line.position, 0.5, {
            y: linepos, 
            delay: this.index*0.1,

        })
      

        // this.indicatorGroup.position.y = 
        // this.indicatorGroup.position.x = this.position.x
        
        




    }

    demoAnim(fC) {


    }


    update(t, fC) {
  


    }

    showBaseMountain(rec) {

        setTimeout(()=>{
            this.indicatorGroup.opacity = 1

            this.baseShowing = true
            this.base.opacity = 1

            console.log(rec)
            this.text.value = `${(rec.HealthScore[0]).toFixed(0)}`

            let value = -(rec.HealthScore[0]/100)*this.two.height/4
            gsap.to(this.base.vertices[1], 0.5, {
                y: value, 
                onUpdate: ()=>{
                    this.indicatorGroup.position.y = this.two.height/2 + this.base.vertices[1].y - 30-this.two.height/8
             

                }
               
            })

     
            
        },1500)

    }

    hideBaseMountain(){
        this.base.opacity = 0
        this.indicatorGroup.opacity = 0



    }

    resize() {
        this.calculatePosition()
        this.redrawTriangles();
        this.redrawLine()
        this.setData(this.data)
       


    }


    // Events
    handleClick() {
        ////console.log(this.path.id)
    }
}