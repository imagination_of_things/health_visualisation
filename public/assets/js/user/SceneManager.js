// This is the class that handles the scene, its state, 
//Contains sub groups for the elements
//Each of the elements are defined as other classes

class SceneManager {
    constructor(two, nodeClickCallback) {
        this.two = two;
        this.nodeLayer = this.two.makeGroup()
        this.margin = 50;
        this.nodeRadius = this.two.height / 3
        if (this.two.height > this.two.width) {
            this.nodeRadius = this.two.width / 3

        }
        this.nodeClickCallback = nodeClickCallback

        this.data = null
        this.top3Labels = []
        this.top3Recommendations = []

        

        this.displayState = -1;


        this.treeDrawData = calculateTreeDrawMetrics(this.two)

        // Make Tree and Branches
 


        //Make Habit Nodes
        this.nodeElems = [];
        this.habitNodes = this.makeNodes()





        // Make leaves
        this.leafLayer = this.two.makeGroup()
        this.leafNodes = this.makeLeaves()
        // setupEventListeners
        let listener1Interval = setInterval(() => {
            this.setupEventListenersForNodes()

            if (this.nodeElems) {

                window.clearInterval(listener1Interval)
            }
        }, 500)

        this.treeLayer = this.two.makeGroup()
        this.treeGraph = new TreeGraph(this.treeLayer, this.two, masterColors.treeBlue)
        this.treeGraph.display()
        //Make Mountains
        this.mountainLayer = this.two.makeGroup()
        this.mountainState = 4;
        this.mountains = this.makeMountains()


        // Make TextNodes
        this.textLayer = this.two.makeGroup()
        this.textNodes = this.makeText()



    }

    // 0 raw state
    // 1 health data in

    // 2 drawer open
    setDisplayState(dS) {
        this.displayState = dS
        if(this.displayState === 0) {


            this.leafNodes.forEach((leaf)=>{
                leaf.setDisplayState(dS)
            })

            this.mountains.forEach((mountain)=>{
                mountain.setDisplayState(dS)
            })

            this.textNodes.forEach((text)=>{
                text.setDisplayState(0)
            })

        } else if (this.displayState === 1){
            this.hideShowHabitNodes(true)
            this.leafNodes.forEach((leaf)=>{
                leaf.setDisplayState(1)
            })

            this.mountains.forEach((mountain)=>{
                mountain.setDisplayState(1)
            })

            this.textNodes.forEach((text)=>{
                text.setDisplayState(1)
            })


        }

    }


    setData(userData, healthScoreData) {
        this.data = healthScoreData[0];
        this.CurrentHealthScore = this.data.CurrentHealthScore[0];

        this.recommendations = this.data["AllCases"]

        let top3Recommendations = []
        let top3Labels = []

        let maxGainsForEachHabit = []

        for(let i=0; i<this.recommendations.length; i++){

            console.log(this.recommendations[i].Variable[0])



            //If the health gain is above the current health score
            if((this.recommendations[i].HealthScore[0] - this.CurrentHealthScore) > 0){


                //If value not already there
                if(maxGainsForEachHabit.filter(e => e.Variable === this.recommendations[i].Variable[0]).length <= 0){

                    maxGainsForEachHabit.push({"Variable": this.recommendations[i].Variable[0], "branchIndex": habitMappings[this.recommendations[i].Variable[0]]['branchIndex'], "BranchLevel": this.recommendations[i].HealthScore[0]})
                }


            } else {

                if(maxGainsForEachHabit.filter(e => e.Variable === this.recommendations[i].Variable[0]).length <= 0){
                    maxGainsForEachHabit.push({"Variable": this.recommendations[i].Variable[0],"branchIndex": habitMappings[this.recommendations[i].Variable[0]]['branchIndex'], "BranchLevel": this.CurrentHealthScore})
                }

            }

          

            //If variable recommendation not already there
            if(top3Labels.indexOf(this.recommendations[i].Variable[0]) < 0 && top3Labels.length<3){
                top3Recommendations.push(this.recommendations[i])
                top3Labels.push(this.recommendations[i].Variable[0])

            }
        }

        this.top3Labels = top3Labels
        this.top3Recommendations = top3Recommendations

        console.log(userData)
        
        // Get current health score set to Leaves + User Mountain

        this.leafNodes.forEach((leaf, index)=>{
            leaf.setData(userData[leaf.key], this.CurrentHealthScore/100, false)
        })

        this.mountains[0].setData(this.CurrentHealthScore/100)
        this.mountains[1].setData(0.6)
        this.textNodes[1].setData(60)

        this.textNodes[0].setData(this.CurrentHealthScore.toFixed(0))
        for(let i=2; i<this.mountains.length; i++){
            let randomData =10+Math.random()*80
            //console.log(randomData)
            this.mountains[i].setData(randomData/100)


            this.textNodes[i].setData((randomData).toFixed(0))

            if(i===3){
                console.log(userData.sex)
                this.textNodes[3].setData((randomData).toFixed(0), userData.sex)

            }
        }


        this.habitNodes.forEach((habit, index)=>{
            let indexOfRecommendation = top3Labels.indexOf(habit.key)
            let recommendationValue = userData[habit.key]
            //console.log(top3Labels, habit.key)
            //console.log(indexOfRecommendation)
            habit.setData(indexOfRecommendation, recommendationValue)

           

        })

        // Set Branches Max Score

        this.treeGraph.setBranchData(maxGainsForEachHabit)




    }
    

 

    makeMountains() {
        let mountains = []

        for (let i = 0; i < 4; i++) {
            let x = this.two.width / 13
            let mountain = new MountainNode(i, this.two, this.mountainLayer)
            mountain.setState(3);
            mountains.push(mountain)
        }


        return mountains
    }

    makeNodes() {
        let nodes = []
        let branchDrawingData = this.treeDrawData.branches

        for (let i = 0; i < 5; i++) {
            // if()
            let node = new HabitNode(new Two.Vector(-this.nodeRadius * Math.sin(branchDrawingData[i].theta), branchDrawingData[i].y1 - this.nodeRadius * Math.cos(branchDrawingData[i].theta)), masterColors.treeBlue, this.nodeLayer, this.two, i);
            nodes.push(node)
        }

        for (let i = 5; i < 11; i++) {
            // if()
            let node = new HabitNode(new Two.Vector(this.nodeRadius * Math.sin(branchDrawingData[i].theta), branchDrawingData[i].y1 - this.nodeRadius * Math.cos(branchDrawingData[i].theta)), masterColors.treeBlue, this.nodeLayer, this.two, i);
            nodes.push(node)
        }

        return nodes

    }

    makeLeaves() {
        let leaves = []
        for (let i = 0; i < 11; i++) {
            if (i < 5) {
                let leaf = new LeafNode(this.treeDrawData, 0.0, masterColors.semigood, this.two, this.leafLayer, i)
                leaves.push(leaf)

            } else if (i === 5) {
                let leaf = new LeafNode(this.treeDrawData, 0.0, 'rgba(255,255,255,0.0)', this.two, this.leafLayer, i)
                leaves.push(leaf)

            } else {
                let leaf = new LeafNode(this.treeDrawData, 0.0, masterColors.semigood, this.two, this.leafLayer, i)
                leaves.push(leaf)
            }

        }

        return leaves;

    }


    makeText() {
        let texts = []

        for (let i = 0; i < 4; i++) {
            let text = new TextNode(i, this.two, this.textLayer)
            texts.push(text)
        }


        return texts

    }



    update(frameCount) {
        let t = performance.now();


        this.habitNodes.forEach((node, index) => {
            if (node) {
                node.update(t)

            }
        })
        this.treeGraph.update(t, frameCount)
   
        this.leafNodes.forEach((leaf)=>{
            leaf.update(t, frameCount)
        })

     
        this.mountains.forEach((mountain, index) => {
            mountain.update(t, frameCount);



        })

     
    
    }

    hideShowHabitNodes(bool){
        //console.log("habit labels toggle")
        this.habitNodes.forEach((habit, index)=>{
            habit.hideShowHabitNodes(bool)
        })
    }

    toggleHabitLabels(bool) {
        this.habitNodes.forEach((habit, index)=>{
            habit.toggleLabels(bool)
        })
    }

    hideIndicator() {

        if(this.mountains){
            this.mountains[0].indicatorGroup.opacity = 0
        }
    }

    resize(dimensions) {
        //console.log("resize")
        this.two.width = dimensions.w
        this.two.height = dimensions.h

        this.nodeRadius = this.two.height / 3.2
        if (this.two.height > this.two.width) {
            this.nodeRadius = this.two.width / 4

        }


        this.treeGraph.resize()
        

        //CalculateMetrics
        //Mountains 
        // //console.log()
        this.textNodes.forEach((node, index)=>{
            node.resize()
        })



        this.habitNodes.forEach((node, index)=>{
            let direction = 1;
            if(index<5){
                direction = -1
            }
         

            let newPosition = new Two.Vector(direction*this.nodeRadius * Math.sin(this.treeGraph.drawData.branches[index].theta), this.treeGraph.drawData.branches[index].y1 - this.nodeRadius * Math.cos(this.treeGraph.drawData.branches[index].theta))
            node.resize(newPosition)
        })

        this.leafNodes.forEach((node, index)=>{
            node.resize(this.treeGraph.drawData)
        })
        this.mountains.forEach((mountain, index)=>{
            mountain.resize()
        })


    }



    setupEventListenersForNodes() {
        //console.log("calling")
        this.habitNodes.forEach((node, index) => {
            if (node.overlayCircle["_renderer"].elem) {
                $(node.overlayCircle["_renderer"].elem).hover(this.nodeHoverIn.bind(this), this.nodeHoverOut.bind(this))

                $(node.overlayCircle["_renderer"].elem).click(this.nodeClickHandler.bind(this))
                this.nodeElems.push(node.overlayCircle["_renderer"].elem)

            }



        })
    }


    // Node HANDLERS - 

    nodeHoverIn(e) {
        e.preventDefault()
        let nodeItself = this.habitNodes.find(obj => {
            return obj.key === e.target.id
        })

        console.log(nodeItself)
        gsap.to(nodeItself.circle, 0.1, {
            scale: 1.2
        })

    }

    nodeHoverOut(e) {
        e.preventDefault()
        let nodeItself = this.habitNodes.find(obj => {
            return obj.key === e.target.id
        })
        gsap.to(nodeItself.circle, 0.1, {
            scale: 1
        })
        console.log(nodeItself)

    }

    nodeClickHandler(e) {
        e.preventDefault()
        let clickedNode = this.nodeLayer.children.find(obj => {
            return obj.id === e.target.id
        })

        let index = this.top3Labels.indexOf(clickedNode.id)>=0
        
     
        this.nodeClickCallback(clickedNode.id, this.top3Labels.indexOf(clickedNode.id), this.top3Recommendations)


        // Check to update Mountain node for new health score

        if(index) {
           
            console.log(index, this.top3Recommendations[this.top3Labels.indexOf(clickedNode.id)])
            this.mountains[0].showBaseMountain(this.top3Recommendations[this.top3Labels.indexOf(clickedNode.id)])


        }  else {
            this.mountains[0].hideBaseMountain()

        }

      

    }
}