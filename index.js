var express = require('express')
var path = require('path')
var serveStatic = require('serve-static')
const axios = require('axios').default;
const {
    response
} = require('express');

require('dotenv').config()

const headers = {
    'Content-Type': 'application/json',
}
// console.log(process.env.API_USER)

var app = express()

app.use(serveStatic(path.join(__dirname, 'public')))

var port = process.env.PORT || 3000; // set our port
var router = express.Router(); // get an instance of the express Router

// Get Access Token

router.get("/auth_token", (req, res, next) => {
    let token = '';
    axios.post('https://prod-diamonds.tno.nl/api/authenticate', {
            username: process.env.API_USER,
            password: process.env.API_PASS
        }, {
            headers: headers
        })
        .then(response => {
            //    console.log(response)
            token = response.data.token
            res.json({
                auth: token, 
                status: "success"
            })

        })
        .catch(error => {

                console.error(error)

                res.json({
                    auth: '',
                    status: error
                })

            }

        );






});

app.use('/middleware', router);




console.log("server listening on port:" + port)
app.listen(3000)